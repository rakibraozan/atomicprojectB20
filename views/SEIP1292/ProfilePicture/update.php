<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP1292\Utility\Utility;
use App\Bitm\SEIP1292\ProfilePicture\ImageUploader;


//Utility::d($_FILES);
$profilePicture= new ImageUploader();
$singleInfo= $profilePicture->prepare($_POST)->view();

if((isset($_FILES['image']))&& !empty($_FILES['image']['name'])){
    $imageName=time().$_FILES['image']['name'];
    $temporaryLocation= $_FILES['image']['tmp_name'];
    unlink($_SERVER['DOCUMENT_ROOT'].'/AtomicProjectB20/Resource/Images/'.$singleInfo->images);
    move_uploaded_file($temporaryLocation,'../../../Resource/Images/'. $imageName);
    $_POST['image']= $imageName;


}


//Utility::d($_POST);


$profilePicture->prepare($_POST)->update();

